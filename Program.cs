﻿using System;
using UsersProj.Readers;

namespace UsersProj
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                switch (args.Length)
                {
                case 0:
                    Console.WriteLine("Please, provide args as <FULL-PATH> (string) in addition you can specify: <LOW-AGE-LIMIT> (int), and <UPPER-AGE-LIMIT> (int).");
                    return;
                case 1:
                case 2:
                        ReadFile(args[0], null, null);
                        break;
                case 3:
                        if (args[1].ToLower() == "true" || args[1].ToLower() == "false")
                            ReadFileSingleRestrictedAge(args[0], args[1].ToLower(), int.Parse(args[2]));
                        else
                        {
                            ReadFile(args[0], int.Parse(args[1]), int.Parse(args[2]));
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error found: {0}", e);
            }
        }

        public static bool IsJson(string path)
        {
            string ending = path.ToLower().Substring(path.Length - 5);
            if (ending.Contains(".json")) return true;
            else if (ending.Contains(".xml")) return false;
            throw new ArgumentException("Could not open file at specified path.");
        }

        public static void ReadFile(string path, int? ageFrom, int? ageTo)
        {
            if (ageTo == null && ageFrom == null)
            {
                if (IsJson(path))
                {
                    JsonUserReader reader = new JsonUserReader(path);
                    reader.ExtractData();
                }
                else if (!IsJson(path))
                {
                    XmlUserReader reader = new XmlUserReader(path);
                    reader.ExtractData();
                }
            }
            else if (ageTo > 0 && ageFrom > 0)
            {
                if (IsJson(path))
                {
                    JsonUserReader reader = new JsonUserReader(path, (int)ageFrom, (int)ageTo);
                    reader.ExtractData();
                }
                else if (!IsJson(path))
                {
                    XmlUserReader reader = new XmlUserReader(path, (int)ageFrom, (int)ageTo);
                    reader.ExtractData();
                }
            }
            else
            {
                throw new ArgumentException("Could not open file at specified path with specified arguments.");
            }
        }

        public static void ReadFileSingleRestrictedAge(string path, string isAgeFrom, int ageRestriction)
        {
            if (isAgeFrom == "false")
            {
                if (IsJson(path))
                {
                    JsonUserReader reader = new JsonUserReader(path, false, ageRestriction);
                    reader.ExtractData();
                }
                else if (!IsJson(path))
                {
                    XmlUserReader reader = new XmlUserReader(path, false, ageRestriction);
                    reader.ExtractData();
                }
            }
            else if (isAgeFrom == "true")
            {
                if (IsJson(path))
                {
                    JsonUserReader reader = new JsonUserReader(path, true, ageRestriction);
                    reader.ExtractData();
                }
                else if (!IsJson(path))
                {
                    XmlUserReader reader = new XmlUserReader(path, true, ageRestriction);
                    reader.ExtractData();
                }
            }
            else
            {
                throw new ArgumentException("Could not open file at specified path with specified arguments.");
            }
        }
    }
}

//Xml console tests
//All
//XmlUserReader reader = new XmlUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.xml");
// From 20 to 35 yo
//XmlUserReader reader = new XmlUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.xml", 20, 35);
// From 20 yo
//XmlUserReader reader = new XmlUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.xml", true, 20);
// To 20 yo
//XmlUserReader reader = new XmlUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.xml", false, 20);


// Corrupted json:
//JsonUserReader reader = new JsonUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.json");
//All
//JsonUserReader reader = new JsonUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users_good.json");
// From 20 to 35 yo
//JsonUserReader reader = new JsonUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users_good.json", 20, 35);
// From 20 yo
//JsonUserReader reader = new JsonUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users_good.json", true, 20);
// To 20 yo
//JsonUserReader reader = new JsonUserReader("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users_good.json", false, 20);

//reader.ReadFromFile();
//return;

// isJSon tests
//IsJson("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users_good.json");
//IsJson("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\users.xml");
//IsJson("X:\\Coding\\Capgemini\\GMT\\UsersProj\\UsersProj\\Data\\user");
