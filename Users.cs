﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UsersProj
{
    class Users
    {
        [JsonProperty("users")]
        public List<User> UsersList { get; set; }
    }
}
