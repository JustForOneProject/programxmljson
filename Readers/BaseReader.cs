﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UsersProj.Readers
{
    abstract class BaseReader
    {
        private readonly string _path;
        private readonly int _ageFrom;
        private readonly int _ageTo;

        public abstract void ExtractData();
    }
}
