﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using System.Linq;

namespace UsersProj.Readers
{
    class JsonUserReader : BaseReader
    {
        private readonly string _path;
        private readonly int _ageFrom, _ageTo;
        public JsonUserReader(string path, int ageFrom, int ageTo)
        {
            _path = path;
            _ageFrom = ageFrom;
            _ageTo = ageTo;
        }
        public JsonUserReader(string path, bool from, int ageRestriction)
        {
            _path = path;
            if (from)
            {
                _ageTo = -1;
                _ageFrom = ageRestriction;
            }
            else
            {
                _ageTo = ageRestriction;
                _ageFrom = -1;
            }
        }
        public JsonUserReader(string path)
        {
            _path = path;
            _ageFrom = -1;
            _ageTo = -1;
        }

        public override void ExtractData()
        {
            var errors = new List<string>();

            try
            {
                var jsonSettings = new JsonSerializerSettings
                {
                    Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        errors.Add(args.ErrorContext.Error.Message);
                        args.ErrorContext.Handled = true;
                        Console.WriteLine("Json deserialize exception: {0}", args.ErrorContext.Error.Message);
                    }
                };
                var users = JsonConvert.DeserializeObject<Users>(StringifyJsonFile(), jsonSettings);

                if (users != null)
                {
                    IEnumerable<User> checkedUsers = users.UsersList.Where(x => x.Age != null).Where(x => x.Name != null);
                    IEnumerable<User> result;

                    if (_ageFrom < 0 && _ageTo < 0)
                    {
                        result = checkedUsers;
                    }
                    else if (_ageFrom < 0 && _ageTo >= 0)
                    {
                        result = checkedUsers.Where(x => int.Parse(x.Age) <= _ageTo)
                            .Select(x => new User()
                            {
                                Age = x.Age,
                                Name = x.Name
                            });
                    }
                    else if (_ageFrom >= 0 && _ageTo < 0)
                    {
                        result = checkedUsers.Where(x => int.Parse(x.Age) >= _ageFrom)
                            .Select(x => new User()
                            {
                                Age = x.Age,
                                Name = x.Name
                            });
                    }
                    else
                    {
                        result = checkedUsers.Where(x => int.Parse(x.Age) <= _ageTo)
                            .Where(x => int.Parse(x.Age) >= _ageFrom)
                            .Select(x => new User()
                            {
                                Age = x.Age,
                                Name = x.Name
                            });
                    }

                    Console.WriteLine("User Name | User Age");
                    foreach (var user in result)
                        Console.WriteLine($"{user.Name}|{user.Age}");
                }
                else
                {
                    Console.WriteLine("JSON file is corrupted. Please specify proper Json file path.");
                    return;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception caught: {0}", e);
            }
        }

        private string StringifyJsonFile()
        {
            string fileString;
            using (StreamReader file = File.OpenText(_path))
            {
                fileString = file.ReadToEnd();
            }
            return fileString;
        }

    }
}
