﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace UsersProj.Readers
{
    class XmlUserReader : BaseReader
    {
        private readonly string _path;
        private readonly int _ageFrom;
        private readonly int _ageTo;
        public XmlUserReader(string path, int ageFrom, int ageTo)
        {
            _path = path;
            _ageFrom = ageFrom;
            _ageTo = ageTo;
        }
        public XmlUserReader(string path, bool from, int ageRestriction)
        {
            _path = path;
            if (from)
            {
                _ageTo = -1;
                _ageFrom = ageRestriction; 
            }
            else
            {
                _ageTo = ageRestriction;
                _ageFrom = -1;
            }
        }
        public XmlUserReader(string path)
        {
            _path = path;
            _ageFrom = -1;
            _ageTo = -1;
        }
        public override void ExtractData()
        {
            try
            {
                var xDoc = XDocument.Load(_path);
                var users = xDoc.Descendants("user").Where(x => x.Element("age").Value != null).Where(x => x.Element("name").Value != null);
                IEnumerable<User> checkedUsers;

                if (_ageFrom < 0 && _ageTo < 0)
                {
                    checkedUsers = users.Select(x => new User()
                    {
                        Age = x.Element("age").Value,
                        Name = x.Element("name").Value
                    });
                }
                else if (_ageFrom < 0 && _ageTo >= 0)
                {
                    checkedUsers = users.Where(x => (int)x.Element("age") <= _ageTo)
                        .Select(x => new User()
                        {
                            Age = x.Element("age").Value,
                            Name = x.Element("name").Value
                        });
                }
                else if (_ageFrom >= 0 && _ageTo < 0)
                {
                    checkedUsers = users.Where(x => (int)x.Element("age") >= _ageFrom)
                        .Select(x => new User()
                        {
                            Age = x.Element("age").Value,
                            Name = x.Element("name").Value
                        });
                }
                else
                {
                    checkedUsers = users.Where(x => (int)x.Element("age") <= _ageTo)
                        .Where(x => (int)x.Element("age") >= _ageFrom)
                        .Select(x => new User()
                        {
                            Age = x.Element("age").Value,
                            Name = x.Element("name").Value
                        });
                }

                Console.WriteLine("User Name | User Age");
                foreach (var user in checkedUsers)
                    Console.WriteLine($"{user.Name}|{user.Age}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e}");
            }
        }
    }
}